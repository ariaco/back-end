## Description

AriaCo project

## Running the app

```bash
# development
$ git clone --recurse-submodules  https://gitlab.com/ariaco/back-end.git
$ docker-compose up -d
